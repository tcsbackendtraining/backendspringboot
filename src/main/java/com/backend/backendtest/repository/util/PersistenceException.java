package com.backend.backendtest.repository.util;

public class PersistenceException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4774168634938439668L;

	public PersistenceException(String msg) {
		super(msg);
	}

}
