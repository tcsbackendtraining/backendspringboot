package com.backend.backendtest.repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.backend.backendtest.model.Customer;
import com.backend.backendtest.repository.util.PersistenceException;

@Component("customerRepository")
public class CustomerRepositoryImpl implements CustomerRepository{

	@Override
	public List<Customer> getCustomers(String name, int age) {
		List<Customer> customerList = new ArrayList<>();
		Customer c1 = new Customer();
		customerList.add(c1);
		Customer c2 = new Customer();
		customerList.add(c2);
		Customer c3 = new Customer();
		customerList.add(c3);
		return customerList;
	}

	@Override
	public Customer getCustomerById(int id) throws PersistenceException {
		Customer customer = new Customer();
		customer.setCustomerId(id);
		customer.setBirthDay(LocalDate.now());
		customer.setFirstName("Original_Customer_Name");
		customer.setLastName("Original_Customer_Lastname");
		return customer;
	}

}
