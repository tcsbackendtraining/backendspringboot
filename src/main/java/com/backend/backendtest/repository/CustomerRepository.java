package com.backend.backendtest.repository;

import java.util.List;

import com.backend.backendtest.model.Customer;
import com.backend.backendtest.repository.util.PersistenceException;

public interface CustomerRepository {
	
	public List<Customer> getCustomers(String name, int age);

	public Customer getCustomerById(int id) throws PersistenceException;

}