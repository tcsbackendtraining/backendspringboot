package com.backend.backendtest.controller;

import java.util.ArrayList;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.backend.backendtest.model.Customer;
import com.backend.backendtest.model.CustomerPro;

@RestController
@RequestMapping("rest")
public class MyController {

	Logger logger = LogManager.getLogger(MyController.class);
	
	@GetMapping(path="getCustomers",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getCustomers(){
		ArrayList customers = new ArrayList<Customer>();
		customers.add(new Customer());
		customers.add(new Customer());
		customers.add(new Customer());
		customers.add(new Customer());
		customers.add(new Customer());
		return new ResponseEntity<ArrayList>(customers, HttpStatus.OK);
	}
	
	@GetMapping(path="getCustomersByDoB",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getCustomersByDoB(@PathParam("dob") String dob){
		System.out.println("DoB = "+dob);
		logger.info("DoB = "+dob);
		ArrayList customers = new ArrayList<Customer>();
		customers.add(new Customer());
		customers.add(new Customer());
		customers.add(new Customer());
		customers.add(new Customer());
		customers.add(new Customer());
		return new ResponseEntity<ArrayList>(customers, HttpStatus.OK);
	}
	
	@PostMapping(path="saveCustomer",
			produces=MediaType.APPLICATION_JSON_VALUE,
			consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CustomerPro> saveCustomer(@RequestBody CustomerPro customer){
		logger.info(customer);
		return new ResponseEntity<CustomerPro>(customer, HttpStatus.OK);
	}
	
	@PostMapping(path="saveCustomerValid",
			produces=MediaType.APPLICATION_JSON_VALUE,
			consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CustomerPro> saveCustomerValid(@Valid @RequestBody CustomerPro customer, BindingResult result){
		logger.info(customer);
		if(result.hasErrors())
			logger.error("SHIT"+result.getAllErrors());
		else
			logger.info("saved!");
		return new ResponseEntity<CustomerPro>(customer, HttpStatus.OK);
	}

}
