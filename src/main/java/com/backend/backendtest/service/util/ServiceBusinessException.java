package com.backend.backendtest.service.util;

public class ServiceBusinessException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1943252481052747424L;

	public ServiceBusinessException(String msg) {
		super(msg);
	}
}
