package com.backend.backendtest.service;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.backend.backendtest.model.Customer;
import com.backend.backendtest.repository.CustomerRepository;
import com.backend.backendtest.repository.util.PersistenceException;
import com.backend.backendtest.service.util.ServiceBusinessException;

public class CustomerServiceImpl implements CustomerService{
	
	private static final Logger logger = LogManager.getLogger(CustomerServiceImpl.class);
	
	@Autowired
	@Qualifier("customerRepository")
	private CustomerRepository customerRepository;

	@Override
	public List<Customer> getCustomers(String name, int age) throws ServiceBusinessException {
		if(null == name)
			throw new ServiceBusinessException("Name is null.");
		if("".equals(name))
			throw new ServiceBusinessException("Name is Empty.");
		if(age <= 1)
			throw new ServiceBusinessException("Age is not allowed.");
		logger.info("All looks good name:"
				.concat(name)
				.concat(" age:")
				.concat(String.valueOf(age)));
		List<Customer> customerList = customerRepository.getCustomers(name, age);
		customerList.stream().forEach(customer -> customer.setFirstName(customer.getFirstName().toUpperCase()));
		logger.info("Result list:".concat(customerList.toString()));
		return customerList;
	}

	@Override
	public Customer getCustomerById(int id) throws ServiceBusinessException {
		if(id <= 1)
			throw new ServiceBusinessException("Id not valid.");
		try {
			return customerRepository.getCustomerById(id);
		} catch (PersistenceException e) {//Cambiamos el tipo de exception para no mezclar las exceptions entre layers.
			logger.error(e);
			throw new ServiceBusinessException(e.getMessage());
		}
	}
	

}
