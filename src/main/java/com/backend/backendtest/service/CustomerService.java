package com.backend.backendtest.service;

import java.util.List;

import com.backend.backendtest.model.Customer;
import com.backend.backendtest.service.util.ServiceBusinessException;

public interface CustomerService {
	public List<Customer> getCustomers(String name, int age) throws ServiceBusinessException;
	public Customer getCustomerById(int id) throws ServiceBusinessException;
}
