package com.backend.backendtest.component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

@Component("requestTimeInterceptor")
public class RequestTimeInterceptor extends HandlerInterceptorAdapter{
	
	private static final Logger logger = LogManager.getLogger(RequestTimeInterceptor.class);


	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		request.setAttribute("startTime", System.currentTimeMillis());
		return true;
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		long time = System.currentTimeMillis() - (long) request.getAttribute("startTime");
		logger.info("Request "
				.concat(request.getMethod())
				.concat(" - ").concat(request.getRequestURI())
				.concat("took ")
				.concat(String.valueOf(time))
				.concat(" ms."));
	}

}
