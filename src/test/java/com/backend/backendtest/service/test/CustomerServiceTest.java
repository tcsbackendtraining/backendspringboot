package com.backend.backendtest.service.test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.core.IsEqual;
import org.hamcrest.core.IsNot;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.backend.backendtest.model.Customer;
import com.backend.backendtest.repository.CustomerRepository;
import com.backend.backendtest.repository.util.PersistenceException;
import com.backend.backendtest.service.CustomerService;
import com.backend.backendtest.service.CustomerServiceImpl;
import com.backend.backendtest.service.util.ServiceBusinessException;

//Quiten la dependencia de Mockito del POM, es una trampa!!!
public class CustomerServiceTest {
	
	/* Este es el Mock (clase que vamos a evadir) que manda a llamar un metodo del Repo dentro
	 * de la clase a Testear (CustomerService)*/
	@Mock
	private CustomerRepository repoMock;
	
	/* Preparamos el Mock de arriba para que evite todas las llamadas al repo original.
	 * Se crea del tipo Interface con una instanca de su implementacion, recuerden que no
	 * podemos crear instancias de Interfaces. */
	@InjectMocks
	private CustomerService service = new CustomerServiceImpl();
	
	/* Iniciamos el circo, este metodo se llama primero que todos los metodos que vamos a
	 * hacer test, si quieren inicializar un objeto que siempre se repita para todos o casi
	 * todos las pruebas este es lugar indicado.
	 */
	@Before
	public void setUp() throws Exception {
	    MockitoAnnotations.initMocks(this);
	}
	
	//All ok happy path
	@Test
	public void getCustomers() throws ServiceBusinessException {
		//Arrange Preparamos todos los objetos que vamos a Mockear
		List<Customer> actualList = new ArrayList<>();
		Customer customer1 = new Customer();
		customer1.setFirstName("Angelica");
		actualList.add(customer1); //Quiero que regrese esta lista en lugar de la original del repo.
		
		/* Con esto decimos que cuando se ejecute el metodo del repo .getCustomers
		 * el cual reciba como parametros cualquier string y cualquier entero
		 * en lugar de mandar a ejecutar el metodo original del repo que lo evite y me regrese la 
		 * lista que preparamos un paso antes */
		Mockito.when(repoMock.getCustomers(Mockito.anyString(), Mockito.anyInt())).thenReturn(actualList);
		
		/* Declaramos la lista de respuesta afuera para poder saber si cambia de valor
		 * al inicio es null, en la parte de Assert preguntamos si NO ES NULL
		 * en caso de que no se ejecute correctamente nos marcaria la prueba como fallida.*/
		List<Customer> responseList  = null;
		
		//Podemos propagar la exepcion en este caso ya que no estamos validando que se genere una.
		responseList = service.getCustomers("Hola", 22);
		
		
		//Assert
		assertNotNull(responseList);//validamos que no sea nula
		/* Para correr todas las pruebas click derecho sobre el nombre de 
		 * esta clase -> Run ass.. -> JUnit Test
		 * Deberia aparecer una Ventana a un lado con el reporte de las pruebas.
		 */
	}
	
	//Name is null
	@Test
	public void getCustomersNameNull() {
		//Arrange
		/* En este caso nos vale si llega a la parte donde ejecuta el repo, vamos a validar 
		 * que se genere una exception por que le mandamos un null en el nombre.
		 */
		
		/* Creamos un objeto de exception que es el padre de nuestra exception y decimos 
		 * que es null para poder evaluarlo despues.
		 */
		Exception expectedException = null;
		
		//Act
		try {
			service.getCustomers(null, 22);
		} catch (Exception e) {
			//La exeption la podemos asignar aqui
			expectedException = e;
		}
		
		//Assert
		assertNotNull(expectedException); //al provocar la exepcion ya no debe ser null
		
		//Podemos validar que la exception realmente sea del tipo ServiceBusinessException
		assertThat(expectedException, instanceOf(ServiceBusinessException.class));
		
		//El mensaje de error que esta mandando la exception
		assertThat(expectedException.getMessage(), IsEqual.equalTo("Name is null."));
		//De hecho deberiamos de cambiar el mensaje de error por una constante. Rifense!
	}
	
	//All ok
	@Test
	public void getCustomerById() throws PersistenceException, ServiceBusinessException {
		//Arrange
		/* El objecto Customer que queremos que regrese en lugar del original generado
		 * por el repo, Verifiquen en el repo que si se llegara a ejecutar les mandaria 
		 * un objeto con todas las propiedades llenas, este que es mock solo tiene el nombre.
		 */
		Customer customerMock = new Customer();
		customerMock.setFirstName("TestName");
		
		/*Inyectamos el Mock cuando se ejecute el metodo getCustomerById del repo
		 */
		Mockito.when(repoMock.getCustomerById(Mockito.anyInt())).thenReturn(customerMock);
		
		/*Declaramos el customer en null para poder comparar en assert si cambia.*/
		Customer customerResponse = null;
		
		//Act
		/*Ejecutamos el metodo del servicio que se va a testear. */
		customerResponse = service.getCustomerById(12);
		
		//Assert
		assertNotNull(customerResponse);//Vamos a ver que no sea Null
		assertThat(customerResponse.getFirstName(), IsNot.not(""));// que no sea ""
		assertThat(customerResponse.getFirstName(), IsNot.not("Original_Customer_Name"));// que no sea igual al del repo
	}
	
	//Queremos que el repo mande una exception, como que no se puede conectar a la BD
	@Test
	public void getCustomerByIdPersistenceExcpetion() throws PersistenceException {
		//Arrange
		/* Creamos un Mock el cual diga que en lugar de ejecutar el metodo getCustomerById del repo
		 * nos lance una excepion
		 */
		String msg = "We are fucked, there is not DB Connection.";
		Mockito.when(repoMock.getCustomerById(Mockito.anyInt())).thenThrow(new PersistenceException(msg));
		
		/*Declaramos el customer en null para poder comparar en assert si cambia.*/
		Exception exeptionResponse = null;
		
		//Act
		/*Ejecutamos el metodo del servicio que se va a testear pero asignamos la exception
		 * para poder validarla */
		try {
			service.getCustomerById(12);
		} catch (Exception e) {
			// Asignamos la exception
			exeptionResponse = e;
		}
		
		//Assert
		assertNotNull(exeptionResponse); //al provocar la exepcion ya no debe ser null
		
		/* Podemos validar que la exception realmente sea del tipo ServiceBusinessException
		 * A pesar de que al crear el mock que nos va a mandar la PersistenceException nos pide 
		 * que agreguemos un throws eso nunca va a pasar por que el metodo que realmente estamos
		 * probando es del Service dentro del cual se cambia el tipo de exeption para no mezclar 
		 * entre las capas del proyecto.
		 */
		assertThat(exeptionResponse, instanceOf(ServiceBusinessException.class));
		
		//El mensaje de error que esta mandando la exception
		assertThat(exeptionResponse.getMessage(), IsEqual.equalTo(msg));
	}

}
